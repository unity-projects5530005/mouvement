using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class parcours : MonoBehaviour
{
    public int point;
    public List<GameObject> spots;

    public NavMeshAgent player;

    // Start is called before the first frame update
    void Start()
    {
        player.SetDestination(spots[point].transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(Vector3.Distance(player.transform.position, spots[point].transform.position));
        if (Vector3.Distance(player.transform.position, spots[point].transform.position) < 0.5)
        {
            point = (point + 1) % spots.Count;
            player.SetDestination(spots[point].transform.position);
        }
    }
}
